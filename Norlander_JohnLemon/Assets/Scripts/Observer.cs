﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Observer : MonoBehaviour
{
    public Transform player;

    bool m_IsPlayerInRange;

    public WaypointPatrol patrolScript;


    void OnTriggerEnter(Collider other)
    {
        if (other.transform == player)
        { print("entered");
            m_IsPlayerInRange = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.transform == player)
        {
            print("exited");
            m_IsPlayerInRange = false;
            patrolScript.SetAggro(false); 
        }
    }

    void Update()
    {
        if (m_IsPlayerInRange)
        {
            Vector3 direction = player.position - transform.position + Vector3.up;
            Ray ray = new Ray(transform.position, direction);
            RaycastHit raycastHit;

            if (Physics.Raycast(ray, out raycastHit))
            {
                if (raycastHit.collider.transform == player)
                {
                   
                    patrolScript.SetAggro(true);
                }
            }
        }
    }
}
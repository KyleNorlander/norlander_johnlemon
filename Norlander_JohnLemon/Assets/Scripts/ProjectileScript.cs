﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileScript : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {

        print("Collide");
        if (other.gameObject.CompareTag("Enemy"))
        {
            print("here");
            EnemyHealth eHealth = other.gameObject.GetComponent<EnemyHealth>();
            if (eHealth != null)
                eHealth.TakeDamage(1);
            Destroy(gameObject);
        }

        
        
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WaypointPatrol : MonoBehaviour
{
    public NavMeshAgent navMeshAgent;
    public Transform[] waypoints;

    Transform player;
    bool aggro;

    public GameEnding gameEnding;

    int m_CurrentWaypointIndex;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform; 
        navMeshAgent.SetDestination(waypoints[0].position);
    }

    void Update()
    {
        if (!aggro && navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance)
        {
            m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length;
            navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
        }
    }

    public void SetAggro (bool set)
    {
        print(set);
        aggro = set;
        if (aggro)
            navMeshAgent.SetDestination(player.position);
        else
            navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.other.transform == player)
            gameEnding.CaughtPlayer();
    }
}